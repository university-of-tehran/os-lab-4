#include "types.h"
#include "stat.h"
#include "user.h"
int main()
{
  barrierInit();
  int pid = fork();
  if(pid != 0)
  {
    int pid2 = fork();
    if(pid2 == 0)
      sleep(500);
    else
      sleep(100);
  }
  barrierCheckIn();
  wait();
  wait();
  exit();
}
