#include "spinlock.h"
#define MAX_LIST_SIZE 50
struct barrier {
  char *name;
  uint locked;
  struct spinlock lk;
  int pidList[MAX_LIST_SIZE];
  int numOfProcs;
  int listIndex;
  int pid;
};
