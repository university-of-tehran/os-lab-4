// Mutual exclusion spin locks.

#include "types.h"
#include "defs.h"
#include "param.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "barrier.h"
#include "spinlock.h"

void
acqSleep(struct barrier *lk)
{
  acquire(&lk->lk);
  while (lk->locked) {
    sleep(lk, &lk->lk);
  }
  lk->locked = 1;
  lk->pid = myproc()->pid;
  release(&lk->lk);
}

void
relSleep(struct barrier *lk)
{
  acquire(&lk->lk);
  lk->locked = 0;
  lk->pid = 0;
  wakeup(lk);
  release(&lk->lk);
}

void
bInit(struct barrier *br, char *name, int _numOfProcs)
{
  br->name = name;
  br->locked = 0;
  br->pid = 0;
  initlock(&br->lk, "sleep lock");
  for(int i = 0; i < MAX_LIST_SIZE ; i++)
  {
      br->pidList[i] = 0;
  }
  br->numOfProcs = _numOfProcs;
  br->listIndex = 0;
}

void
bCheckIn(struct barrier *br)
{
  if(br->listIndex != br->numOfProcs-1)
  {
    br->pidList[br->listIndex] = myproc()->pid;
    br->listIndex = br->listIndex + 1;
    cprintf("Process with PID: %d Sleeping\n", myproc()->pid);
    acqSleep(br);
  }
  else
  {
    for(int j = 0; j < br->numOfProcs-1 ; j++)
    {
      cprintf("Process %d Waking Up process %d\n", myproc()->pid, br->pidList[j]);
      relSleep(br);
    }
  }
}
